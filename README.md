# 🚀 Gatsby Tailwind

The gatsby tailwind is a tailwind-configured template, which means you won't have to add files or go through the hassle of configuring tailwind css in your project.

## Installation

```bash
  git clone https://github.com/amrohan/Gatsby-Tailwind.git
```

## Run Locally

Go to the project directory

```bash
  cd Gatsby-Tailwind
```

## Install dependencies

```bash
  npm install
  or
  yarn install
```

### Start the server

```bash
  npm start
  or
  yarn start
```
