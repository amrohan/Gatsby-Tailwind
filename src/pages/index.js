import * as React from "react";

// markup
const IndexPage = () => {
  return (
    <div>
      <h1 className="text-red-500 text-2xl text-center">Gatsby-Portfolio</h1>
    </div>
  );
};

export default IndexPage;
